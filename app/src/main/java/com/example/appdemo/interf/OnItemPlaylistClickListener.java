package com.example.appdemo.interf;

public interface OnItemPlaylistClickListener {
    void onPlaylistClick(String playlistId);
}

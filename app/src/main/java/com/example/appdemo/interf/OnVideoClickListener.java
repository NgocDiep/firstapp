package com.example.appdemo.interf;

import android.os.Bundle;

public interface OnVideoClickListener {
    void onVideoClick(Bundle bundle);
}

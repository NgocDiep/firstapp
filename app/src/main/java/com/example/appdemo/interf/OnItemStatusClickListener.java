package com.example.appdemo.interf;

import com.example.appdemo.json_models.response.Status;

public interface OnItemStatusClickListener {

    void onLikeClick(int position, Status status);

//    void onCommentClick(Status status);

    void onCommentClick(int position, Status status);

    void onEditStatus(Status status);

    void onDeleteStatus(Status status);

    void onDetailImage(int positionAdapter, int positionImage, Status status);
}

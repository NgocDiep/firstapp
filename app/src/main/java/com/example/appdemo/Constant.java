package com.example.appdemo;

public class Constant {
    public static final String GetPositionImage = "GetPositionForDetail";
    public static final String GetPositionAdapterForDetail = "GetPositionAdapterForDetail";
    public static final String GetStatusForDetail = "GetStatus";
    public static final String GetPostIdForComment = "GetPostId";
    public static final String GetNumberComment = "GetNumberComment";
    public static final String GetNumberLike = "GetNumberLike";
    public static final String GetIsLike = "GetIsLike";
    public static final String GetPlaylistId = "GetPlaylistId";
    public static final String API_KEY = "AIzaSyDeQsRAFoVaHBkX3HbZ7yavSrz0Gvi_CKw";

    public static final String GetVideoId = "GetVideoId";
    public static final String GetTitle = "GetTitle";
    public static final String GetDescription = "GetDescription";
    public static final String GetPublishedAt = "GetPublishedAt";
    public static final String GetChannelTitle = "GetChannelTitle";
    public static final int TYPE_YOUTUBE = 0;
    public static final int TYPE_BKHUB = 1;
    public static final String part = "snippet";
    public static final String channelId = "UCjtS-3FFguGtHsf5tPk2ReQ";
}

package com.example.appdemo.json_models.request;

import com.google.gson.annotations.SerializedName;

public class RegisterSendForm {
    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String passWord;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("address")
    private String address;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;

    @SerializedName("avatarUrl")
    private String avatarUrl;

    public RegisterSendForm(String username, String passWord, String fullName, String address, String birthday, String phone, String email, String avatarUrl) {
        this.username = username;
        this.passWord = passWord;
        this.fullName = fullName;
        this.address = address;
        this.birthday = birthday;
        this.phone = phone;
        this.email = email;
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}

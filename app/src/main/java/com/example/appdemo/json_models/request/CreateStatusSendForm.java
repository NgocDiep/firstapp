package com.example.appdemo.json_models.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateStatusSendForm {
    @SerializedName("userId")
    private String userId;

    @SerializedName("content")
    private String content;

    @SerializedName("images")
    private List<String> images;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public CreateStatusSendForm(String userId, String content, List<String> images) {
        this.userId = userId;
        this.content = content;
        this.images = images;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

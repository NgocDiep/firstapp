package com.example.appdemo.json_models.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateGroupChatSendForm {
    @SerializedName("users")
    private List<String> users;


    public CreateGroupChatSendForm(List<String> users) {
        this.users = users;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}

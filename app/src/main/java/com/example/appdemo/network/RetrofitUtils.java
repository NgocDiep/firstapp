package com.example.appdemo.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtils {
    private Retrofit retrofit;
//    private static RetrofitUtils instance;
    public static final int TYPE_YOUTUBE = 0;
    public static final int TYPE_BKHUB = 1;

    private static RetrofitUtils instanceYoutube = new RetrofitUtils(TYPE_YOUTUBE);
    private static RetrofitUtils instanceBkHub = new RetrofitUtils(TYPE_BKHUB);

    public static RetrofitUtils getInstance(int type) {
        switch (type) {
            case TYPE_YOUTUBE:
                return instanceYoutube;
            case TYPE_BKHUB:
                return instanceBkHub;
        }
        return instanceBkHub;
    }

//    private RetrofitUtils() {
//    retrofit = new Retrofit.Builder()
//            .baseUrl(APIStringRoot.API_ROOT)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build();
//    }

//    public static RetrofitUtils getInstance() {
//        if(instance == null){
//            instance = new RetrofitUtils();
//        }
//        return instance;
//    }

    private RetrofitUtils(int type) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
        switch (type) {
            case TYPE_YOUTUBE:
                retrofit = new Retrofit.Builder()
                        .baseUrl(APIStringRoot.API_ROOT_VIDEO_YOUTUBE)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                break;
            case TYPE_BKHUB:
                retrofit = new Retrofit.Builder()
                        .baseUrl(APIStringRoot.API_ROOT_BKHUB)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                break;
            default:
                break;
        }
    }


    public <ServiceClass> ServiceClass createService(Class<ServiceClass> serviceClass){
        return retrofit.create(serviceClass);
    }
}

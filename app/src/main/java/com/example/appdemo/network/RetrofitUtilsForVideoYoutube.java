package com.example.appdemo.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtilsForVideoYoutube {
    private Retrofit retrofit;
    private static RetrofitUtilsForVideoYoutube instance;

    private RetrofitUtilsForVideoYoutube() {
        retrofit = new Retrofit.Builder()
                .baseUrl(APIStringRoot.API_ROOT_VIDEO_YOUTUBE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitUtilsForVideoYoutube getInstance() {
        if(instance == null){
            instance = new RetrofitUtilsForVideoYoutube();
        }
        return instance;
    }

    public <ServiceClass> ServiceClass createService(Class<ServiceClass> serviceClass){
        return retrofit.create(serviceClass);
    }
}

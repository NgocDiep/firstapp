package com.example.appdemo.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.CreateStatusSendForm;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePostActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.view_grey)
    View viewGrey;
    @BindView(R.id.tv_numberImage)
    TextView tvNumberImage;
    @BindView(R.id.iv_ava)
    CircleImageView ivAva;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.edt_content)
    EditText edtContent;
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.item_add_photo)
    LinearLayout itemAddPhoto;
    @BindView(R.id.tv_post)
    TextView tvPost;
    @BindView(R.id.iv_one)
    ImageView ivOne;
    @BindView(R.id.iv_two_1)
    ImageView ivTwo1;
    @BindView(R.id.iv_two_2)
    ImageView ivTwo2;
    @BindView(R.id.iv_three_1)
    ImageView ivThree1;
    @BindView(R.id.iv_three_2)
    ImageView ivThree2;
    @BindView(R.id.iv_three_3)
    ImageView ivThree3;
    @BindView(R.id.iv_four_1)
    ImageView ivFour1;
    @BindView(R.id.iv_four_2)
    ImageView ivFour2;
    @BindView(R.id.iv_four_3)
    ImageView ivFour3;
    @BindView(R.id.iv_four_4)
    ImageView ivFour4;
    @BindView(R.id.iv_five_1)
    ImageView ivFive1;
    @BindView(R.id.iv_five_2)
    ImageView ivFive2;
    @BindView(R.id.iv_five_3)
    ImageView ivFive3;
    @BindView(R.id.iv_five_4)
    ImageView ivFive4;
    @BindView(R.id.iv_five_5)
    ImageView ivFive5;
    UserInfor userInfor;
    String[] uriList;
    int countImage;
    final int MODE_ONE = 0;
    final int MODE_TWO = 1;
    final int MODE_THREE = 2;
    final int MODE_FOUR = 3;
    final int MODE_FIVE = 4;
    private StorageReference storageReference;
    List<String> uriResponseList;
    private RetrofitService retrofitService;
    String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        init();
        addListener();
    }

    private void init() {
        storageReference = FirebaseStorage.getInstance().getReference();
        ButterKnife.bind(this);
        userInfor = RealmContext.getInstance().getUser();
        Glide.with(CreatePostActivity.this).load(userInfor.getAvatar()).into(ivAva);
        tvUsername.setText(userInfor.getFullName());
        Intent intent = getIntent();
        countImage = intent.getIntExtra("GetCountImage", 0);
        uriList = new String[countImage];
        uriList = intent.getStringArrayExtra("GetUri");
        uriResponseList = new ArrayList<>();
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
    }

    private void addListener() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (countImage == 1) {
            viewFlipper.setDisplayedChild(MODE_ONE);
            ivOne.setImageURI(Uri.parse(uriList[0]));
        } else if (countImage == 2) {
            viewFlipper.setDisplayedChild(MODE_TWO);
            ivTwo1.setImageURI(Uri.parse(uriList[0]));
            ivTwo2.setImageURI(Uri.parse(uriList[1]));
        } else if (countImage == 3) {
            viewFlipper.setDisplayedChild(MODE_THREE);
            ivThree1.setImageURI(Uri.parse(uriList[0]));
            ivThree2.setImageURI(Uri.parse(uriList[1]));
            ivThree3.setImageURI(Uri.parse(uriList[2]));
        } else if (countImage == 4) {
            viewFlipper.setDisplayedChild(MODE_FOUR);
            ivFour1.setImageURI(Uri.parse(uriList[0]));
            ivFour2.setImageURI(Uri.parse(uriList[1]));
            ivFour3.setImageURI(Uri.parse(uriList[2]));
            ivFour4.setImageURI(Uri.parse(uriList[3]));
        } else if (countImage == 5) {
            viewFlipper.setDisplayedChild(MODE_FIVE);
            ivFive1.setImageURI(Uri.parse(uriList[0]));
            ivFive2.setImageURI(Uri.parse(uriList[1]));
            ivFive3.setImageURI(Uri.parse(uriList[2]));
            ivFive4.setImageURI(Uri.parse(uriList[3]));
            ivFive5.setImageURI(Uri.parse(uriList[4]));
        } else if (countImage > 5) {
            viewFlipper.setDisplayedChild(MODE_FIVE);
            ivFive1.setImageURI(Uri.parse(uriList[0]));
            ivFive2.setImageURI(Uri.parse(uriList[1]));
            ivFive3.setImageURI(Uri.parse(uriList[2]));
            ivFive4.setImageURI(Uri.parse(uriList[3]));
            ivFive5.setImageURI(Uri.parse(uriList[4]));
            viewGrey.setVisibility(View.VISIBLE);
            tvNumberImage.setVisibility(View.VISIBLE);
            tvNumberImage.setText("+" + (countImage - 4));
        } else {
            viewFlipper.setDisplayedChild(MODE_FIVE);
        }

        itemAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreatePostActivity.this, AddImageOnPostActivity.class);
                startActivity(intent);
            }
        });

        tvPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreatePostActivity.this);
                builder.setMessage("Please wait...");
                builder.show();

                if (uriList != null) {
                    for (int i = 0; i < uriList.length; i++) {
                        uploadImage(Uri.parse(uriList[i]));
                    }
                }
            }
        });
    }

    private void uploadImage(Uri uri) {
        StorageReference ref = storageReference.child("postImage/" + uri.getLastPathSegment());
        UploadTask uploadTask = ref.putFile(uri);

        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    uriResponseList.add(task.getResult().toString());
                }

                if (uriResponseList.size() == countImage) {
                    createPost(uriResponseList);
                }
            }
        });
    }

    private void createPost(List<String> images) {
        content = edtContent.getText().toString();
        CreateStatusSendForm sendForm = new CreateStatusSendForm(userInfor.getUserId(), content, images);
        retrofitService.createPost(sendForm).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Status statusResponse = response.body();
                if (response.code() == 200 && statusResponse != null) {
                    Utils.showToast(CreatePostActivity.this, "Done!");
//                    onBackPressed();
                    Intent intent = new Intent(CreatePostActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utils.showToast(CreatePostActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Utils.showToast(CreatePostActivity.this, "No Internet!");
            }
        });
    }

}

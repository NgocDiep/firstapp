package com.example.appdemo.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenActivity extends AppCompatActivity {
//    TabLayout tabLayout;
//    ViewPager viewPager;
//    AuthenViewPagerAdapter adapter;

    //    final int RC_SIGN_IN = 1;
    public static final int REQUEST_CODE_TOKEN_AUTH = 0;
    @BindView(R.id.btn_login_normal)
    Button btnLoginNormal;
    @BindView(R.id.btn_login_phone)
    Button btnLoginPhone;
    @BindView(R.id.layout_login_gg)
    LinearLayout layoutLoginGg;
    @BindView(R.id.layout_register)
    RelativeLayout layoutRegister;
    private GoogleSignInClient mGoogleSignInClient;
    private RetrofitService retrofitService;
    private static final int RC_GET_AUTH_CODE = 9003;
    public static final String SCOPES = "https://www.googleapis.com/auth/plus.login "
            + "https://www.googleapis.com/auth/drive.file";
    String accountName = null;
    public static int APP_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authen);
        setContentView(R.layout.activity_authen_final);

        validateServerClientID();
        String serverClientId = getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        if (RealmContext.getInstance().getUser() != null) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
//        tabLayout = findViewById(R.id.tabLayout);
//        viewPager = findViewById(R.id.viewPager);
//
//        adapter = new AuthenViewPagerAdapter(getSupportFragmentManager());
//        viewPager.setAdapter(adapter);
//        tabLayout.setupWithViewPager(viewPager);
    }


    private void addListener() {
        btnLoginNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AuthenActivity.this, LoginWithAccountActivity.class);
                startActivity(intent);
            }
        });

        btnLoginPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(AuthenActivity.this, LoginWithPhoneNumberActivity.class));
                phoneLogin(v);
            }
        });

        layoutLoginGg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                signIn();
                getAuthCode();
            }
        });

        layoutRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AuthenActivity.this, RegisterActivity.class));
            }
        });
    }

    public void phoneLogin(final View view) {
        final Intent intent = new Intent(AuthenActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        configurationBuilder.setSMSWhitelist(new String[]{"VN"});
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    private void getAuthCode() {
        // Start the retrieval process for a server auth code.  If requested, ask for a refresh
        // token.  Otherwise, only get an access token if a refresh token has been previously
        // retrieved.  Getting a new access token for an existing grant does not require
        // user consent.
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
    }

//    private void signIn() {
//        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
//        if (requestCode == RC_SIGN_IN) {
//            // The Task returned from this call is always completed, no need to attach
//            // a listener.
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }

        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            final AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
//                    showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                    Log.d("haha", loginResult.getAccessToken().getToken());
                    loginWithPhoneNumber(loginResult.getAccessToken().getToken());
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0, 10));
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
            }

            // Surface the result to your user in an appropriate way.
            Utils.showToast(AuthenActivity.this, toastMessage);
        }

        if (requestCode == RC_GET_AUTH_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()) {
                // [START get_auth_code]
                GoogleSignInAccount account = result.getSignInAccount();
//                String authCode = account.getServerAuthCode();
                accountName = account.getAccount().name;
                // Show signed-in UI.

                // TODO(user): send code to server and exchange for access/refresh/ID tokens.
                // [END get_auth_code]
            }

            AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String token = null;

                    try {
                        token = GoogleAuthUtil.getToken(AuthenActivity.this, accountName, "oauth2:" + SCOPES);
                        Log.i("haha", "Access token retrieved:" + token);

                        loginByGoogle(token);
                    } catch (IOException transientEx) {
                        // Network or server error, try later
                        Log.e("haha", transientEx.toString());
                    } catch (UserRecoverableAuthException e) {
                        // Recover (with e.getIntent())
                        Log.e("haha", e.toString());
                        Intent recover = e.getIntent();

                        startActivityForResult(recover, REQUEST_CODE_TOKEN_AUTH);
                    } catch (GoogleAuthException authEx) {
                        // The call is not ever expected to succeed
                        // assuming you have already verified that
                        // Google Play services is installed.
                        Log.e("haha", authEx.toString());
                    }
                    return token;
                }

                @Override
                protected void onPostExecute(String token) {
                    Log.i("haha", "Access token retrieved:" + token);
                }

            };
            task.execute();
        }
    }

    private void validateServerClientID() {
        String serverClientId = getString(R.string.server_client_id);
        String suffix = ".apps.googleusercontent.com";
        if (!serverClientId.trim().endsWith(suffix)) {
            String message = "Invalid server client ID in strings.xml, must end with " + suffix;
            Log.w("haha", message);
            Utils.showToast(this, message);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.d("haha", account.getIdToken());
            loginByGoogle(account.getIdToken());
            // Signed in successfully, show authenticated UI.

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("haha", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void loginByGoogle(String accessToken) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(AuthenActivity.this).setMessage("Please wait...").show();
            }
        });
        retrofitService.loginWithGoogle(accessToken).enqueue(new Callback<UserInfor>() {
            @Override
            public void onResponse(Call<UserInfor> call, Response<UserInfor> response) {
                UserInfor userResponse = response.body();
                if (response.code() == 200 && userResponse != null) {
                    RealmContext.getInstance().addUser(userResponse);
                    goToHome();
                } else {
                    Utils.showToast(AuthenActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<UserInfor> call, Throwable t) {
                Utils.showToast(AuthenActivity.this, "No Internet!");
            }
        });
    }

    private void loginWithPhoneNumber(String token) {
        retrofitService.loginWithPhoneNumber(token).enqueue(new Callback<UserInfor>() {
            @Override
            public void onResponse(Call<UserInfor> call, Response<UserInfor> response) {
                UserInfor userInfor = response.body();
                if (response.code() == 200 && userInfor != null) {
                    RealmContext.getInstance().addUser(userInfor);
                    Intent intent = new Intent(AuthenActivity.this, HomeActivity.class);
                    startActivity(intent);
                    AccountKit.logOut();
                    finish();
                } else {
                    Utils.showToast(AuthenActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<UserInfor> call, Throwable t) {
                Utils.showToast(AuthenActivity.this, "No Internet");
            }
        });
    }

    private void goToHome() {
        Intent intent = new Intent(AuthenActivity.this, HomeActivity.class);
        startActivity(intent);
        mGoogleSignInClient.signOut();
        finish();
    }
}

package com.example.appdemo.activity;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.ImageListAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnDeleteImageClickListener;
import com.example.appdemo.json_models.request.UpdateStatusSendForm;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePostActivity extends AppCompatActivity implements OnDeleteImageClickListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
//    @BindView(R.id.view_grey)
//    View viewGrey;
//    @BindView(R.id.tv_numberImage)
//    TextView tvNumberImage;
    @BindView(R.id.iv_ava)
    CircleImageView ivAva;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.edt_content)
    EditText edtContent;
//    @BindView(R.id.btn_float)
//    FloatingActionButton btnAdd;
    @BindView(R.id.item_add_photo)
    LinearLayout itemAddPhoto;
    @BindView(R.id.tv_ok)
    TextView tvOk;
    UserInfor userInfor;
    private StorageReference storageReference;
    List<String> uriResponseList;
    private RetrofitService retrofitService;
    String content, contentFromIntent, postId;
    List<String> imagesFromIntent;
    String newContent;
    @BindView(R.id.rv_images)
    RecyclerView recyclerView;
    ImageListAdapter adapter;
    List<Uri> imageList, imageListAdd;
    List<String> imageListSend;
    String[] listPermissions = null;
    public static final int REQUEST_PERMISSION_CODE = 1;
    public static final int REQUEST_GET_IMAGE_CODE = 2;
    String imageEncoded;
    List<String> imagesEncodedList;
    Status status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_post);
        init();
        addListener();
    }

    private void init() {
        storageReference = FirebaseStorage.getInstance().getReference();
        ButterKnife.bind(this);
        userInfor = RealmContext.getInstance().getUser();
        Glide.with(UpdatePostActivity.this).load(userInfor.getAvatar()).into(ivAva);
        tvUsername.setText(userInfor.getFullName());

        Intent intentUpdatePost = getIntent();
        status = (Status)intentUpdatePost.getSerializableExtra(Constant.GetStatusForDetail);
        contentFromIntent = status.getContent();
        imagesFromIntent = status.getImages();
        postId = status.getPostId();
        edtContent.setText(contentFromIntent);

        imageList = new ArrayList<>();
        for(int i=0;i<imagesFromIntent.size();i++){
            imageList.add(Uri.parse(imagesFromIntent.get(i)));
        }

        imageListAdd = new ArrayList<>();
        imageListSend = new ArrayList<>();
        uriResponseList = new ArrayList<>();
        adapter = new ImageListAdapter(imageList, this::onDeleteImage);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UpdatePostActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
    }

    private void addListener() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        itemAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ensurePermission();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdatePostActivity.this);
                builder.setMessage("Please wait...");
                builder.show();

                newContent = edtContent.getText().toString();
                if(imageListAdd.size() == 0){
                    updatePost(newContent);
                } else {
                    for(int i=0;i<imageListAdd.size();i++){
                        uploadImage(imageListAdd.get(i));
                    }
                }
            }
        });
    }

    private boolean checkPermission(Context context, String[] listPermissions) {
        if (context != null && listPermissions != null) {
            for (String permission : listPermissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void ensurePermission() {
        listPermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermission(UpdatePostActivity.this, listPermissions)) {
            openGallery();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissions, REQUEST_PERMISSION_CODE);
            }
        }
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), REQUEST_GET_IMAGE_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (checkPermission(UpdatePostActivity.this, listPermissions)) {
                openGallery();
            } else {
                Utils.showToast(UpdatePostActivity.this, "Request is denied!");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            // When an Image is picked
            if (requestCode == REQUEST_GET_IMAGE_CODE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {
                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    cursor.close();

                    imageListAdd.add(mImageUri);
                    imageList.add(mImageUri);
                    adapter.notifyDataSetChanged();
                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            imageListAdd.add(uri);
                            imageList.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                            adapter.notifyDataSetChanged();
                        }
                        Log.v("LOG_TAG", "Selected Images " + imageListAdd.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage(Uri uri) {
        StorageReference ref = storageReference.child("postImage/" + uri.getLastPathSegment());
        UploadTask uploadTask = ref.putFile(uri);

        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    uriResponseList.add(task.getResult().toString());
                }

                if (uriResponseList.size() == imageListAdd.size()) {
                    imageListSend.addAll(uriResponseList);
                    updatePost(newContent);
                }
            }
        });
    }

    private void updatePost(String newContent) {
        for(int i = 0;i<imageList.size();i++){
            if(imagesFromIntent.contains(imageList.get(i).toString())) {
                imageListSend.add(imageList.get(i).toString());
            }
        }
        UpdateStatusSendForm sendForm = new UpdateStatusSendForm(userInfor.getUserId(), newContent, imageListSend);
        retrofitService.updateStatus(postId, sendForm).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if (response.code() == 200) {
                    Utils.showToast(UpdatePostActivity.this, "Done!");
                    Intent intent = new Intent(UpdatePostActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utils.showToast(UpdatePostActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Utils.showToast(UpdatePostActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onDeleteImage(Uri uri) {
        if(imageListAdd.contains(uri)){
            imageListAdd.remove(uri);
        }
        imageList.remove(uri);
        adapter.notifyDataSetChanged();
    }
}

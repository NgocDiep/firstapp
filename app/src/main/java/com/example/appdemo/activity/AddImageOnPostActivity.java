package com.example.appdemo.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.appdemo.R;
import com.example.appdemo.adapter.ImageListAdapter;
import com.example.appdemo.interf.OnDeleteImageClickListener;
import com.example.appdemo.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class AddImageOnPostActivity extends AppCompatActivity implements OnDeleteImageClickListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.btn_float)
    FloatingActionButton btnAdd;
    @BindView(R.id.rv_image)
    RecyclerView recyclerView;
    @BindView(R.id.iv_ok)
    ImageView ivOk;
    ImageListAdapter imageAdapter;
    List<Uri> imageList;
    String[] listPermissions = null;
    public static final int REQUEST_PERMISSION_CODE = 1;
    public static final int REQUEST_GET_IMAGE_CODE = 2;
    String imageEncoded;
    List<String> imagesEncodedList;
    List<String> uriResponseList;
    String[] uris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_cover);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);

        imageList = new ArrayList<>();
        imageAdapter = new ImageListAdapter(imageList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AddImageOnPostActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(imageAdapter);
        uriResponseList = new ArrayList<>();
    }

    private void addListener() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ensurePermission();
            }
        });

        ivOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("haha", valueOf(imageList.size()));
                uris = new String[imageList.size()];
                for (int i = 0; i < imageList.size(); i++) {
                    uris[i] = imageList.get(i).toString();
                }
                Intent intent = new Intent(AddImageOnPostActivity.this, CreatePostActivity.class);
                intent.putExtra("GetCountImage", imageList.size());
                intent.putExtra("GetUri", uris);
                startActivity(intent);
//                finish();
            }
        });
    }

    private void ensurePermission() {
        listPermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermission(AddImageOnPostActivity.this, listPermissions)) {
            openGallery();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissions, REQUEST_PERMISSION_CODE);
            }
        }
    }

    private boolean checkPermission(Context context, String[] listPermissions) {
        if (context != null && listPermissions != null) {
            for (String permission : listPermissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void openGallery() {
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_PICK);
//        intent.setType("image/*");
//        startActivityForResult(intent, REQUEST_GET_IMAGE_CODE);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), REQUEST_GET_IMAGE_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (checkPermission(AddImageOnPostActivity.this, listPermissions)) {
                openGallery();
            } else {
                Utils.showToast(AddImageOnPostActivity.this, "Request is denied!");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            // When an Image is picked
            if (requestCode == REQUEST_GET_IMAGE_CODE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {
                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    cursor.close();

                    imageList.add(mImageUri);
                    imageAdapter.notifyDataSetChanged();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            imageList.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                            imageAdapter.notifyDataSetChanged();
                        }
                        Log.v("LOG_TAG", "Selected Images" + imageList.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        if (imageList.size() > 0) {
            ivOk.setVisibility(View.VISIBLE);
        } else ivOk.setVisibility(View.INVISIBLE);

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDeleteImage(Uri uri) {
        imageList.remove(uri);
        imageAdapter.notifyDataSetChanged();

        if (imageList.size() > 0) {
            ivOk.setVisibility(View.VISIBLE);
        } else ivOk.setVisibility(View.INVISIBLE);
    }
}

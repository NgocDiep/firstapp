package com.example.appdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.DetailPostAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.LikeStatusSendForm;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.String.valueOf;

public class DetailPostActivity extends AppCompatActivity{
    List<String> imageList;
    DetailPostAdapter adapter;
    @BindView(R.id.iv_ava)
    CircleImageView ivAva;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_datetime)
    TextView tvDatetime;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_numberLike)
    TextView tvNumberLike;
    @BindView(R.id.tv_numberComment)
    TextView tvNumberComment;
    @BindView(R.id.rv_image_detail)
    RecyclerView recyclerView;
    @BindView(R.id.tv_menu)
    TextView tvMenu;
    Status status;
    int position;
    UserInfor userInfor;
    @BindView(R.id.itemLike)
    LinearLayout itemLike;
    @BindView(R.id.itemComment)
    LinearLayout itemComment;
    @BindView(R.id.view_flipper_ivLike)
    ViewFlipper viewFlipperIvLike;
    @BindView(R.id.view_flipper_tvLike)
    ViewFlipper viewFlipperTvLike;
    RetrofitService retrofitService;
    public static int REQUEST_CODE_DETAIL = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_post);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        Intent intent = getIntent();
        status = (Status) intent.getSerializableExtra(Constant.GetStatusForDetail);
        position = intent.getIntExtra(Constant.GetPositionImage, 0);

        tvUsername.setText(status.getAuthorName());
        tvDatetime.setText(status.getCreateDate());
        tvContent.setText(status.getContent());
        tvNumberLike.setText(valueOf(status.getNumberLike()));
        tvNumberComment.setText(valueOf(status.getNumberComment()));
        Glide.with(this).load(status.getAuthorAvatar()).into(ivAva);
        userInfor = RealmContext.getInstance().getUser();

        if(status.isLike()){
            viewFlipperIvLike.setDisplayedChild(1);
            viewFlipperTvLike.setDisplayedChild(1);
        } else {
            viewFlipperIvLike.setDisplayedChild(0);
            viewFlipperTvLike.setDisplayedChild(0);
        }

        if (status.getAuthor().equals(userInfor.getUserId())) {
            tvMenu.setVisibility(View.VISIBLE);
        } else {
            tvMenu.setVisibility(View.INVISIBLE);
        }

        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        imageList = new ArrayList<>();
        imageList = status.getImages();
        adapter = new DetailPostAdapter(imageList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DetailPostActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        if (!status.getImages().isEmpty()) {
            recyclerView.smoothScrollToPosition(position);
//            recyclerView.scrollToPosition(position);
//            linearLayoutManager.scrollToPositionWithOffset(position, 0);
        }
    }

    private void addListener(){
        itemLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likePost(status);
                if (status.isLike()) {
                    viewFlipperIvLike.setDisplayedChild(0);
                    tvNumberLike.setText(valueOf(Integer.parseInt(tvNumberLike.getText().toString()) - 1));
                    viewFlipperTvLike.setDisplayedChild(0);
                } else {
                    viewFlipperIvLike.setDisplayedChild(1);
                    tvNumberLike.setText(valueOf(Integer.parseInt(tvNumberLike.getText().toString()) + 1));
                    viewFlipperTvLike.setDisplayedChild(1);
                }
            }
        });

        itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailPostActivity.this, CommentActivity.class);
                intent.putExtra(Constant.GetPostIdForComment, status.getPostId());
//                intent.putExtra(Constant.GetUserIdForComment, status.);
                intent.putExtra(Constant.GetStatusForDetail, status);
//                startActivity(intent);
                startActivityForResult(intent, CommentActivity.REQUEST_CODE_DETAIL);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CommentActivity.REQUEST_CODE_DETAIL && resultCode == Activity.RESULT_OK && data != null){
            tvNumberComment.setText(valueOf(data.getIntExtra(Constant.GetNumberComment, 0)));
            tvNumberLike.setText(data.getStringExtra(Constant.GetNumberLike));
            if(data.getBooleanExtra(Constant.GetIsLike, false)){
                viewFlipperIvLike.setDisplayedChild(1);
                viewFlipperTvLike.setDisplayedChild(1);
            } else {
                viewFlipperIvLike.setDisplayedChild(0);
                viewFlipperTvLike.setDisplayedChild(0);
            }
        }
    }

    private void likePost(Status status) {
        LikeStatusSendForm sendForm = new LikeStatusSendForm(userInfor.getUserId(), status.getPostId());
        retrofitService.likePost(sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    status.setLike(!status.isLike());
                    if (status.isLike()) {
                        status.setNumberLike(status.getNumberLike() + 1);
                    } else {
                        status.setNumberLike(status.getNumberLike() - 1);
                    }
                } else {
                    Utils.showToast(DetailPostActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(DetailPostActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constant.GetNumberComment, tvNumberComment.getText().toString());
        intent.putExtra(Constant.GetIsLike, status.isLike());
        intent.putExtra(Constant.GetNumberLike, tvNumberLike.getText().toString());
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }
}

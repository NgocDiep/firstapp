package com.example.appdemo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.UpdateProfileSendForm;
import com.example.appdemo.json_models.response.ProfileUser;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity {
    @BindView(R.id.edt_fullName) EditText edtFullName;
    @BindView(R.id.edt_address) EditText edtAddress;
    @BindView(R.id.edt_DoB) EditText edtDoB;
    @BindView(R.id.edt_phone) EditText edtPhone;
    @BindView(R.id.btn_cancel) Button btnCancel;
    @BindView(R.id.btn_ok) Button btnOk;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    String updateFullName, updateAddress, updateBirthday, updatePhone;
    private RetrofitService retrofitService;
    UserInfor userInfor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        userInfor = RealmContext.getInstance().getUser();
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
    }

    private void addListener(){
        getProfile(userInfor.getUsername(), userInfor.getUserId());

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFullName = edtFullName.getText().toString();
                updateAddress = edtAddress.getText().toString();
                updateBirthday = edtDoB.getText().toString();
                updatePhone = edtPhone.getText().toString();

                UpdateProfileSendForm sendForm = new UpdateProfileSendForm(updateFullName, updateAddress, updateBirthday, updatePhone);
                retrofitService.updateProfile(userInfor.getUserId(), sendForm).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if(response.code() == 200){
                            Utils.showToast(UpdateProfileActivity.this, "Done!");
                            onBackPressed();
                        } else {
                            Utils.showToast(UpdateProfileActivity.this, "Fail!");
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Utils.showToast(UpdateProfileActivity.this, "No Internet!");
                    }
                });
            }
        });
    }

    private void getProfile(String username, String userId) {
        retrofitService.getProfileUser(username, userId).enqueue(new Callback<ProfileUser>() {
            @Override
            public void onResponse(Call<ProfileUser> call, Response<ProfileUser> response) {
                ProfileUser profileUser = response.body();
                if (response.code() == 200 && profileUser != null) {
                    edtFullName.setText(profileUser.getFullName());
                    edtAddress.setText(profileUser.getAddress());
                    edtDoB.setText(profileUser.getBirthday());
                    edtPhone.setText(profileUser.getPhone());
                } else {
                    Utils.showToast(UpdateProfileActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<ProfileUser> call, Throwable t) {
                Utils.showToast(UpdateProfileActivity.this, "No Internet!");
            }
        });
    }
}

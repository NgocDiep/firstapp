package com.example.appdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.RegisterSendForm;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.edt_username)
    EditText edtUsername;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.edt_confirm_pass)
    EditText edtConfirmPass;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_fullName)
    EditText edtFullName;
    @BindView(R.id.edt_phone)
    EditText edtPhone;
    @BindView(R.id.btn_register)
    Button btnRegister;
    private RetrofitService retrofitService;
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    static final int MODE_PROGRESS_BAR = 1;
    static final int MODE_BUTTON = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
    }

    private void addListener() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFlipper.setDisplayedChild(MODE_PROGRESS_BAR);
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                String confirmPass = edtConfirmPass.getText().toString();
                String fullName = edtFullName.getText().toString();
                String address = "", birthday = "", avatarUrl = "";
                String phone = edtPhone.getText().toString();
                String email = edtEmail.getText().toString();
                if(!password.equals(confirmPass)){
                    Utils.showToast(RegisterActivity.this, "Password isn't match!");
                } else {
                    register(username, password, fullName, address, birthday, phone, email, avatarUrl);
                }
            }
        });
    }

    private void register(String username, String password, String fullName, String address,
                          String birthday, String phone, String email, String avatarUrl){
        RegisterSendForm sendForm = new RegisterSendForm(username, password, fullName, address, birthday, phone, email, avatarUrl);
        retrofitService.register(sendForm).enqueue(new Callback<UserInfor>() {
            @Override
            public void onResponse(Call<UserInfor> call, Response<UserInfor> response) {
                UserInfor userResponse = response.body();
                if(response.code() == 200 && userResponse != null){
                    RealmContext.getInstance().addUser(userResponse);
                    Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                    startActivity(intent);
                    RegisterActivity.this.finish();
                } else {
                    Utils.showToast(RegisterActivity.this, "Account already exists!");
                }
                viewFlipper.setDisplayedChild(MODE_BUTTON);
            }

            @Override
            public void onFailure(Call<UserInfor> call, Throwable t) {
                Utils.showToast(RegisterActivity.this, "No Internet!");
            }
        });
    }
}

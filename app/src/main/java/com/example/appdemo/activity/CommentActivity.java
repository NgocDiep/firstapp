package com.example.appdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.CommentAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.CommentStatusSendForm;
import com.example.appdemo.json_models.request.LikeStatusSendForm;
import com.example.appdemo.json_models.response.Comment;
import com.example.appdemo.json_models.response.CommentCreate;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.String.valueOf;

public class CommentActivity extends AppCompatActivity {
    ViewFlipper viewFlipper;
    RecyclerView recyclerView;
    private RetrofitService retrofitService;
    EditText edtComment;
    ImageView ivSend, ivAva;
    CommentAdapter commentAdapter;
    UserInfor user;
    ArrayList<Comment> commentList;
    final int MODE_NO_DATA = 1;
    final int MODE_RECYCYCLEVIEW = 2;
    String postId;
    public final static int REQUEST_CODE = 1;
    public final static int REQUEST_CODE_DETAIL = 2;
    TextView tvNumberLike;
    ViewFlipper viewFlipperLike;
    Status status;
    final int MODE_LIKE = 0;
    final int MODE_DONT_LIKE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        user = RealmContext.getInstance().getUser();

        init();
        if (user != null) {
//            Glide.with(this).load(user.getAvatar()).into(ivAva);
            getAllComment(status.getPostId());
        }
        addListener();
    }

    private void init() {
        Intent intent = getIntent();
        status = (Status) intent.getSerializableExtra(Constant.GetStatusForDetail);
        postId = status.getPostId();

        viewFlipper = findViewById(R.id.view_flipper);

        viewFlipperLike = findViewById(R.id.view_flipper_like);
        recyclerView = findViewById(R.id.rv_cmt);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
//        ivAva = findViewById(R.id.iv_ava);
        ivSend = findViewById(R.id.iv_send);
        edtComment = findViewById(R.id.edt_comment);
        tvNumberLike = findViewById(R.id.tv_numberLike);
        tvNumberLike.setText(valueOf(status.getNumberLike()));
        if (status.isLike()) {
            viewFlipperLike.setDisplayedChild(MODE_LIKE);
        } else {
            viewFlipperLike.setDisplayedChild(MODE_DONT_LIKE);
        }

        commentList = new ArrayList<>();
        commentAdapter = new CommentAdapter(commentList);
        recyclerView.setAdapter(commentAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void addListener() {
        viewFlipperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewFlipperLike.getDisplayedChild() == MODE_LIKE) {
                    viewFlipperLike.setDisplayedChild(MODE_DONT_LIKE);
                    tvNumberLike.setText(valueOf(Integer.parseInt(tvNumberLike.getText().toString()) - 1));
                } else {
                    viewFlipperLike.setDisplayedChild(MODE_LIKE);
                    tvNumberLike.setText(valueOf(Integer.parseInt(tvNumberLike.getText().toString()) + 1));
                }
                likePost(status);
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = edtComment.getText().toString();
                if (!content.isEmpty()) {
                    commentStatus(content);
                } else {
                    Utils.showToast(CommentActivity.this, "Comment mustn't be empty!");
                }
            }
        });
    }

    private void likePost(Status status) {
        LikeStatusSendForm sendForm = new LikeStatusSendForm(user.getUserId(), status.getPostId());
        retrofitService.likePost(sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    status.setLike(!status.isLike());
                    if (status.isLike()) {
                        status.setNumberLike(status.getNumberLike() + 1);
                    } else {
                        status.setNumberLike(status.getNumberLike() - 1);
                    }
                } else {
                    Utils.showToast(CommentActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(CommentActivity.this, "No Internet!");
            }
        });
    }

    private void commentStatus(String content) {
        CommentStatusSendForm sendForm = new CommentStatusSendForm(user.getUserId(), postId, content);
        retrofitService.commentStatus(sendForm).enqueue(new Callback<CommentCreate>() {
            @Override
            public void onResponse(Call<CommentCreate> call, Response<CommentCreate> response) {
                CommentCreate res = response.body();
                if (response.code() == 200 && res != null) {
                    Utils.showToast(CommentActivity.this, "Done!");
                    getAllComment(postId);
                    edtComment.setText("");
                } else {
                    Utils.showToast(CommentActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<CommentCreate> call, Throwable t) {
                Utils.showToast(CommentActivity.this, "No Internet!");
            }
        });
    }

    private void getAllComment(String postId) {
        retrofitService.getAllComment(postId).enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                ArrayList<Comment> comments = (ArrayList<Comment>) response.body();
                if (response.code() == 200 && comments != null) {
                    commentList.clear();
                    commentList.addAll(comments);
                    commentAdapter.notifyDataSetChanged();
                    viewFlipper.setDisplayedChild(MODE_RECYCYCLEVIEW);
                } else {
                    viewFlipper.setDisplayedChild(MODE_NO_DATA);
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Utils.showToast(CommentActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constant.GetNumberComment, commentList.size());
        intent.putExtra(Constant.GetIsLike, status.isLike());
        intent.putExtra(Constant.GetNumberLike, tvNumberLike.getText().toString());
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }
}

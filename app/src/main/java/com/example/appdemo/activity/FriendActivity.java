package com.example.appdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.FriendAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnItemFriendClickListener;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendActivity extends AppCompatActivity implements OnItemFriendClickListener {
    ViewFlipper viewFlipper;
    RecyclerView recyclerView;
    UserInfor user;

    ArrayList<UserInfor> friendList;
    FriendAdapter friendAdapter;
    RetrofitService retrofitService;
    final int CODE_OK = 200;

    final int MODE_NO_DATA = 1;
    final int MODE_NO_INTERNET = 2;
    final int MODE_RECYCLEVIEW = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        init();
        getAllFriend();
    }

    private void init() {
        viewFlipper = findViewById(R.id.view_flipper);
        recyclerView = findViewById(R.id.rv_list_friend);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        user = RealmContext.getInstance().getUser();
        friendList = new ArrayList<>();
        friendAdapter = new FriendAdapter(friendList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(FriendActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(friendAdapter);
    }

    private void getAllFriend() {
        retrofitService.getAllFriend(user.getUserId()).enqueue(new Callback<ArrayList<UserInfor>>() {
            @Override
            public void onResponse(Call<ArrayList<UserInfor>> call, Response<ArrayList<UserInfor>> response) {
                ArrayList<UserInfor> friends = response.body();
                if (response.code() == CODE_OK && friends != null && !friends.isEmpty()) {
                    friendList.clear();
                    friendList.addAll(friends);
                    friendAdapter.notifyDataSetChanged();
                    viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
                } else {
                    viewFlipper.setDisplayedChild(MODE_NO_DATA);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserInfor>> call, Throwable t) {
                viewFlipper.setDisplayedChild(MODE_NO_INTERNET);
            }
        });
    }

    @Override
    public void viewProfileFriend(UserInfor userInfor) {
        Intent intent = new Intent(FriendActivity.this, ProfileFriendActivity.class);
        intent.putExtra("GetUserId", userInfor.getUserId());
        intent.putExtra("GetUsername", userInfor.getUsername());
        intent.putExtra("GetAvatarUrl", userInfor.getAvatar());
        startActivity(intent);
    }
}

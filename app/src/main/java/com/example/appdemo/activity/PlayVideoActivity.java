package com.example.appdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.AllVideoOfPlaylistAdapter;
import com.example.appdemo.interf.OnVideoClickListener;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, OnVideoClickListener {
    @BindView(R.id.video)
    YouTubePlayerView videoPlayer;
    @BindView(R.id.tv_channel_title)
    TextView tvChannelTitle;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_publish)
    TextView tvPublish;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    YouTubePlayer mPlayer;
    @BindView(R.id.view_flipper_detail)
    ViewFlipper viewFlipperDetail;
    @BindView(R.id.view_flipper_video)
    ViewFlipper viewFlipperVideo;
    @BindView(R.id.rv_video)
    RecyclerView recyclerView;
    @BindView(R.id.layout_detail)
    LinearLayout layoutDetail;
    final int MODE_EXPAND = 0;
    final int MODE_SHORTEN = 1;
    ArrayList<JsonObject> videoArrayList;
    AllVideoOfPlaylistAdapter videoAdapter;
    private RetrofitService retrofitService;
    final static int MODE_RECYCLEVIEW = 1;
    String playlistId;

    String videoId, publishedAt, title, description, channelTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        init();
        videoPlayer.initialize(Constant.API_KEY, this);
        getAllVideoOfPlaylist(Constant.part, playlistId, Constant.API_KEY);
        addListener();
    }

    private void init(){
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        playlistId = intent.getStringExtra(Constant.GetPlaylistId);

        if (bundle != null) {
            videoId = bundle.getString(Constant.GetVideoId);
            publishedAt = bundle.getString(Constant.GetPublishedAt);
            title = bundle.getString(Constant.GetTitle);
            description = bundle.getString(Constant.GetDescription);
            channelTitle = bundle.getString(Constant.GetChannelTitle);
        }
        tvTitle.setText(title);
        tvChannelTitle.setText(channelTitle);
        tvPublish.setText("Publish at " + publishedAt);
        tvDescription.setText(description);

        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_YOUTUBE).createService(RetrofitService.class);

        videoArrayList = new ArrayList<>();
        videoAdapter = new AllVideoOfPlaylistAdapter(videoArrayList, this::onVideoClick);
        LinearLayoutManager layoutManager = new LinearLayoutManager(PlayVideoActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(videoAdapter);
    }

    private void addListener(){
        viewFlipperDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewFlipperDetail.getDisplayedChild() == MODE_EXPAND){
                    viewFlipperDetail.setDisplayedChild(MODE_SHORTEN);
                    layoutDetail.setVisibility(View.VISIBLE);
                } else {
                    viewFlipperDetail.setDisplayedChild(MODE_EXPAND);
                    layoutDetail.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            //Áp dụng để phát một video mới
            mPlayer = youTubePlayer;
            //dùng hàm này để phát video mới, truyền vào video Id
            //mPlayer.cueVideo("videoId");
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            youTubePlayer.loadVideo(videoId);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    private void getAllVideoOfPlaylist(String part, String playlistId, String key) {
        retrofitService.getAllVideoOfPlaylist(part, playlistId, key).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if (response.code() == 200 && jsonObject != null) {
                    JsonArray jsonPlaylistArray = jsonObject.getAsJsonArray("items");
                    for (int i = 0; i < jsonPlaylistArray.size(); i++) {
                        videoArrayList.add(jsonPlaylistArray.get(i).getAsJsonObject());
                    }
                    videoAdapter.notifyDataSetChanged();
                    viewFlipperVideo.setDisplayedChild(MODE_RECYCLEVIEW);
                } else {
                    Utils.showToast(PlayVideoActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utils.showToast(PlayVideoActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onVideoClick(Bundle bundle) {
        mPlayer.cueVideo(bundle.getString(Constant.GetVideoId));
        tvTitle.setText(bundle.getString(Constant.GetTitle));
        tvDescription.setText(bundle.getString(Constant.GetDescription));
        tvPublish.setText(bundle.getString(Constant.GetPublishedAt));
    }
}

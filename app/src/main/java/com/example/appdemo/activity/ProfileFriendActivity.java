package com.example.appdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.StatusAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnItemStatusClickListener;
import com.example.appdemo.json_models.request.LikeStatusSendForm;
import com.example.appdemo.json_models.response.ProfileUser;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFriendActivity extends AppCompatActivity implements OnItemStatusClickListener {
    private RetrofitService retrofitService;
    UserInfor userInfor;
    SliderLayout sliderLayout;
    TextView tvUsername, tvAddress, tvDoB, tvPhone;
    ViewFlipper viewFlipper;
    final int MODE_NODATA = 1;
    final int MODE_RECYCLEVIEW = 2;
    ArrayList<Status> statusArrayList;
    RecyclerView recyclerView;
    LinearLayout itemAddress, itemPhone;
    ImageView ivAva;
    int positionDetail = 0, positionCmt = 0;
    List<String> avaList;

    StatusAdapter statusAdapter;
    String userId, username, avatarUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_friend);
        init();
        addListener();
        avaList.add(avatarUrl);
        Glide.with(ProfileFriendActivity.this).load(avatarUrl).into(ivAva);

        getProfile(username, userId);
    }

    private void addListener() {
        ivAva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(avaList.size() != 0){
                    new ImageViewer.Builder(ProfileFriendActivity.this, avaList)
                            .setStartPosition(0)
                            .show();
                }
            }
        });

        itemAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loc = tvAddress.getText().toString();
                Uri addressUri = Uri.parse("geo:0,0?q=" + loc);
                Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);
                startActivity(intent);
            }
        });

        itemPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = tvPhone.getText().toString();
                if (!TextUtils.isEmpty(tel)) {
                    String dial = "tel:" + tel;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        statusAdapter.notifyItemChanged(positionDetail);
    }


    private void init() {
        Intent intent = getIntent();
        userId = intent.getStringExtra("GetUserId");
        username = intent.getStringExtra("GetUsername");
        avatarUrl = intent.getStringExtra("GetAvatarUrl");

        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        sliderLayout = findViewById(R.id.slider);
        tvUsername = findViewById(R.id.tv_username);
        tvAddress = findViewById(R.id.tv_address);
        tvDoB = findViewById(R.id.tv_birthday);
        tvPhone = findViewById(R.id.tv_phone);
        viewFlipper = findViewById(R.id.flipper_status);
        recyclerView = findViewById(R.id.rv_status);
        ivAva = findViewById(R.id.iv_ava);
        itemAddress = findViewById(R.id.item_address);
        itemPhone = findViewById(R.id.item_phone);
        userInfor = RealmContext.getInstance().getUser();
        avaList = new ArrayList<>();

        statusArrayList = new ArrayList<>();

        statusAdapter = new StatusAdapter(this, statusArrayList);
        recyclerView.setAdapter(statusAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProfileFriendActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getProfile(String username, String userId) {
        retrofitService.getProfileUser(username, userId).enqueue(new Callback<ProfileUser>() {
            @Override
            public void onResponse(Call<ProfileUser> call, Response<ProfileUser> response) {
                ProfileUser profileUser = response.body();
                if (response.code() == 200 && profileUser != null) {
                    tvUsername.setText(profileUser.getFullName());
                    tvAddress.setText(profileUser.getAddress());
                    tvDoB.setText(profileUser.getBirthday());
                    tvPhone.setText(profileUser.getPhone());

                    ArrayList<Status> statuses = profileUser.getPostList();
//                    if (statuses != null) {
//                        statusArrayList.clear();
//                        for(int i = profileUser.getPostList().size() - 1; i >= 0; i--){
//                            // Start Error API
//                            Status status = new Status(profileUser.getPostList().get(i).getPostId(),
//                                    profileUser.getPostList().get(i).getAuthor(),
//                                    profileUser.getPostList().get(i).getAuthorName(),
//                                    profileUser.getPostList().get(i).getAuthorAvatar(),
//                                    profileUser.getPostList().get(i).getContent(),
//                                    profileUser.getPostList().get(i).getCreateDate(),
//                                    profileUser.getPostList().get(i).getNumberLike(),
//                                    profileUser.getPostList().get(i).getNumberComment(),
//                                    profileUser.getPostList().get(i).isLike());
//                            profileUser.getPostList().get(i).getImages();
////                            status.setImages(null);
//                            statusArrayList.add(status);
//                            // End Error API
//
//                            //listStatus.add(profileUser.getPostList().get(i));
//                        }
////                        statusArrayList.addAll(statuses);
////                        statusProfileAdapter.notifyDataSetChanged();
//                        statusAdapter.notifyDataSetChanged();
//                        viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
//                    } else {
//                        viewFlipper.setDisplayedChild(MODE_NODATA);
//                    }

                    if (statuses.size() != 0) {
                        statusArrayList.clear();
                        statusArrayList.addAll(statuses);
                        statusAdapter.notifyDataSetChanged();
                        viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
                    } else {
                        viewFlipper.setDisplayedChild(MODE_NODATA);
                    }

                    showSlider(profileUser.getCoverPhoto());
                } else {
                    Utils.showToast(ProfileFriendActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<ProfileUser> call, Throwable t) {
                Utils.showToast(ProfileFriendActivity.this, "No Internet!");
            }
        });
    }

    private void showSlider(String[] coverPhoto) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        for (String url : coverPhoto) {
//            TextSliderView textSliderView = new TextSliderView(getContext());
            DefaultSliderView textSliderView = new DefaultSliderView(ProfileFriendActivity.this);
            textSliderView
                    .image(url)
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .bundle(new Bundle());

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);//đặt hiệu ứng
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);//dấu 3 chấm nằm ở trung tâm ảnh
        sliderLayout.setDuration(4000);//đặt thời gian tự động chuyển ảnh
    }

    private void likePost(int position, Status status) {
        LikeStatusSendForm sendForm = new LikeStatusSendForm(userInfor.getUserId(), status.getPostId());
        retrofitService.likePost(sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    status.setLike(!status.isLike());
                    if (status.isLike()) {
                        status.setNumberLike(status.getNumberLike() + 1);
                    } else {
                        status.setNumberLike(status.getNumberLike() - 1);
                    }
                    statusAdapter.notifyItemChanged(position);
                } else {
                    Utils.showToast(ProfileFriendActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(ProfileFriendActivity.this, "No Internet!");
            }
        });
    }


    @Override
    public void onLikeClick(int position, Status status) {
        likePost(position, status);
    }

    @Override
    public void onCommentClick(int position, Status status) {
        this.positionCmt = position;
        Intent intent = new Intent(ProfileFriendActivity.this, CommentActivity.class);
        intent.putExtra(Constant.GetStatusForDetail, status);
//        startActivity(intent);
        startActivityForResult(intent, CommentActivity.REQUEST_CODE);
    }


    @Override
    public void onEditStatus(Status status) {
    }

    @Override
    public void onDeleteStatus(Status status) {
    }

    @Override
    public void onDetailImage(int positionAdapter, int positionImage, Status status) {
        this.positionDetail = positionAdapter;
        Intent intent = new Intent(ProfileFriendActivity.this, DetailPostActivity.class);
        intent.putExtra(Constant.GetPositionImage, positionImage);
        intent.putExtra(Constant.GetPositionAdapterForDetail, positionAdapter);
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivityForResult(intent, DetailPostActivity.REQUEST_CODE_DETAIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommentActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            statusArrayList.get(positionCmt).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
            statusArrayList.get(positionCmt).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
            statusArrayList.get(positionCmt).setNumberComment(data.getIntExtra(Constant.GetNumberComment, 0));
            statusAdapter.notifyItemChanged(positionCmt);
        }

        if (requestCode == DetailPostActivity.REQUEST_CODE_DETAIL && resultCode == Activity.RESULT_OK && data != null) {
            statusArrayList.get(positionDetail).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
            statusArrayList.get(positionDetail).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
            statusArrayList.get(positionDetail).setNumberComment(Integer.parseInt(data.getStringExtra(Constant.GetNumberComment)));
            statusAdapter.notifyItemChanged(positionDetail);
        }
    }
}

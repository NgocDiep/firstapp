package com.example.appdemo.activity;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.ImageListAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnDeleteImageClickListener;
import com.example.appdemo.json_models.request.UpdateCoverPhotoSendForm;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateCoverActivity extends AppCompatActivity implements OnDeleteImageClickListener {
    @BindView(R.id.btn_float)
    FloatingActionButton btn_add;
    @BindView(R.id.rv_image)
    RecyclerView recyclerView;
    @BindView(R.id.iv_ok)
    ImageView ivOk;
    ImageListAdapter coverAdapter;
    List<Uri> imageList;
    UserInfor userInfor;
    String[] listPermissions = null;
    public static final int REQUEST_PERMISSION_CODE = 1;
    public static final int REQUEST_GET_IMAGE_CODE = 2;
    private StorageReference storageReference;
    List<String> uriResponseList;
    private RetrofitService retrofitService;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    String imageEncoded;
    List<String> imagesEncodedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_cover);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        uriResponseList = new ArrayList<>();
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        userInfor = RealmContext.getInstance().getUser();

        storageReference = FirebaseStorage.getInstance().getReference();
        imageList = new ArrayList<>();
        coverAdapter = new ImageListAdapter(imageList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UpdateCoverActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(coverAdapter);
    }

    private void addListener() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ensurePermission();
            }
        });

//        Log.d("bkhub", valueOf(imageList.size()));
        ivOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(UpdateCoverActivity.this).setMessage("Please wait...").show();
//                imageList.clear();
                for (int i = 0; i < imageList.size(); i++) {
                    uploadImage(imageList.get(i), imageList.size());
                }
                Log.d("bkhub", "aaa");
            }
        });
    }

    private void ensurePermission() {
        listPermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermission(this, listPermissions)) {
            openGallery();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissions, REQUEST_PERMISSION_CODE);
            }
        }
    }

    private boolean checkPermission(Context context, String[] listPermissions) {
        if (context != null && listPermissions != null) {
            for (String permission : listPermissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), REQUEST_GET_IMAGE_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (checkPermission(UpdateCoverActivity.this, listPermissions)) {
                openGallery();
            } else {
                Utils.showToast(UpdateCoverActivity.this, "Request is denied!");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            // When an Image is picked
            if (requestCode == REQUEST_GET_IMAGE_CODE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {
                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    cursor.close();

                    imageList.add(mImageUri);
                    coverAdapter.notifyDataSetChanged();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            imageList.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                            coverAdapter.notifyDataSetChanged();
                        }
                        Log.v("LOG_TAG", "Selected Images" + imageList.size());
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        if (imageList.size() > 0) {
            ivOk.setVisibility(View.VISIBLE);
        } else ivOk.setVisibility(View.INVISIBLE);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage(Uri uri, int size) {
        StorageReference ref = storageReference.child("cover/" + uri.getLastPathSegment());
        UploadTask uploadTask = ref.putFile(uri);

        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    uriResponseList.add(task.getResult().toString());
                }

                if(uriResponseList.size() == size) {
                    updateCoverPhoto();
                }
            }
        });
    }

    private void updateCoverPhoto() {
        UpdateCoverPhotoSendForm sendForm = new UpdateCoverPhotoSendForm(uriResponseList);
        retrofitService.updateCoverPhoto(userInfor.getUserId(), sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    Utils.showToast(UpdateCoverActivity.this, "Done!");
                    finish();
                } else {
                    Utils.showToast(UpdateCoverActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(UpdateCoverActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onDeleteImage(Uri uri) {
        imageList.remove(uri);
        coverAdapter.notifyDataSetChanged();

        if (imageList.size() > 0) {
            ivOk.setVisibility(View.VISIBLE);
        } else ivOk.setVisibility(View.INVISIBLE);
    }
}

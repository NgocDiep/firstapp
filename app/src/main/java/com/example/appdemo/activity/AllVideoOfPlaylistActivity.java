package com.example.appdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.AllVideoOfPlaylistAdapter;
import com.example.appdemo.interf.OnVideoClickListener;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllVideoOfPlaylistActivity extends AppCompatActivity implements OnVideoClickListener {
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.rv_video)
    RecyclerView recyclerView;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title_above)
    TextView tvTitle1;
    @BindView(R.id.tv_title_below)
    TextView tvTitle2;
    @BindView(R.id.btn_float_play)
    FloatingActionButton btnPlay;
    @BindView(R.id.tv_number_video)
    TextView tvNumberVideo;

    ArrayList<JsonObject> videoArrayList;
    AllVideoOfPlaylistAdapter videoAdapter;
    private RetrofitService retrofitService;
    final static int MODE_RECYCLEVIEW = 1;
    String playlistId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_video_of_playlist);
        init();
        getAllVideoOfPlaylist(Constant.part, playlistId, Constant.API_KEY);
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_YOUTUBE).createService(RetrofitService.class);
        Intent intent = getIntent();
        playlistId = intent.getStringExtra(Constant.GetPlaylistId);

        videoArrayList = new ArrayList<>();
        videoAdapter = new AllVideoOfPlaylistAdapter(videoArrayList, this::onVideoClick);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AllVideoOfPlaylistActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(videoAdapter);
    }

    private void addListener(){
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void getAllVideoOfPlaylist(String part, String playlistId, String key) {
        retrofitService.getAllVideoOfPlaylist(part, playlistId, key).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if (response.code() == 200 && jsonObject != null) {
                    JsonArray jsonPlaylistArray = jsonObject.getAsJsonArray("items");
                    for (int i = 0; i < jsonPlaylistArray.size(); i++) {
                        videoArrayList.add(jsonPlaylistArray.get(i).getAsJsonObject());
                    }
                    tvNumberVideo.setText(videoArrayList.size() + " video");
                    JsonObject snippet = jsonPlaylistArray.get(0).getAsJsonObject().get("snippet").getAsJsonObject();
                    tvTitle1.setText(snippet.get("channelTitle").getAsString());
                    tvTitle2.setText(snippet.get("channelTitle").getAsString());
                    videoAdapter.notifyDataSetChanged();
                    viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
                } else {
                    Utils.showToast(AllVideoOfPlaylistActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utils.showToast(AllVideoOfPlaylistActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onVideoClick(Bundle bundle) {
        Intent intent = new Intent(AllVideoOfPlaylistActivity.this, PlayVideoActivity.class);
        intent.putExtras(bundle);
        intent.putExtra(Constant.GetPlaylistId, playlistId);
        startActivity(intent);
    }
}

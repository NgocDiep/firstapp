package com.example.appdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import androidx.appcompat.app.AppCompatActivity;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.request.LoginSendForm;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginWithAccountActivity extends AppCompatActivity {
    @BindView(R.id.edt_username)
    EditText edtUsername;
    @BindView(R.id.edt_pass)
    EditText edtPassword;
    @BindView(R.id.view_flipper_eye)
    ViewFlipper viewFlipperEye;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.layout_register)
    RelativeLayout layoutRegister;
    final int MODE_EYE_VISIBLE = 0;
    final int MODE_EYE_INVISIBLE = 1;
    static final int MODE_PROGRESS_BAR = 1;
    static final int MODE_BUTTON = 0;
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    private RetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_account);
        init();
        addListener();
    }

    private void init() {
        ButterKnife.bind(this);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
    }

    private void addListener() {
        viewFlipperEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewFlipperEye.getDisplayedChild() == MODE_EYE_VISIBLE){
                    viewFlipperEye.setDisplayedChild(MODE_EYE_INVISIBLE);
                    edtPassword.setTransformationMethod(null);
                } else {
                    viewFlipperEye.setDisplayedChild(MODE_EYE_VISIBLE);
                    edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                if(username.isEmpty() || password.isEmpty()){
                    Utils.showToast(LoginWithAccountActivity.this, "Username or Password mustn't be empty!");
                } else {
                    login(username, password);
                }
            }
        });

        layoutRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginWithAccountActivity.this, RegisterActivity.class));
            }
        });
    }

    private void login(String username, String password){
        viewFlipper.setDisplayedChild(MODE_PROGRESS_BAR);

        LoginSendForm loginSendForm = new LoginSendForm(username, password);
        retrofitService.login(loginSendForm).enqueue(new Callback<UserInfor>() {
            @Override
            public void onResponse(Call<UserInfor> call, Response<UserInfor> response) {
                UserInfor userInfor = response.body();

                if(response.code()==200 && userInfor != null){
                    RealmContext.getInstance().addUser(userInfor);

                    Intent intent = new Intent(LoginWithAccountActivity.this, HomeActivity.class);
                    startActivity(intent);
                    LoginWithAccountActivity.this.finish();

                } else {
                    Utils.showToast(LoginWithAccountActivity.this, "Username or Password is incorrect!");
                }
                viewFlipper.setDisplayedChild(MODE_BUTTON);
            }

            @Override
            public void onFailure(Call<UserInfor> call, Throwable t) {
                Utils.showToast(LoginWithAccountActivity.this, "No Internet!");
                viewFlipper.setDisplayedChild(MODE_BUTTON);
            }
        });
    }
}

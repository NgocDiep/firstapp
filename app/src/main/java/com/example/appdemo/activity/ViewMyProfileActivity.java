package com.example.appdemo.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.adapter.StatusAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnItemStatusClickListener;
import com.example.appdemo.interf.OnUpdateDialogListener;
import com.example.appdemo.json_models.request.LikeStatusSendForm;
import com.example.appdemo.json_models.response.Avatar;
import com.example.appdemo.json_models.response.ProfileUser;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMyProfileActivity extends AppCompatActivity implements OnItemStatusClickListener, OnUpdateDialogListener {
    private RetrofitService retrofitService;
    UserInfor userInfor;
    SliderLayout sliderLayout;
    TextView tvUsername, tvAddress, tvDoB, tvPhone;
    ViewFlipper viewFlipper;
    final int MODE_NODATA = 1;
    final int MODE_RECYCLEVIEW = 2;
    ArrayList<Status> statusArrayList;
    RecyclerView recyclerView;
    private StorageReference storageReference;
    String address, DoB, phone;
    SwipeRefreshLayout refreshLayout;

    StatusAdapter statusAdapter;
    ImageView ivAva, newfeedAva, ivCamera, ivUpdateCover;
    List<String> avatarUrl;
    TextView tvMenu, tvPost;
    LinearLayout itemAddress, itemPhone;
    String[] listPermissions = null;
    public static final int REQUEST_PERMISSION_CODE = 1;
    public static final int REQUEST_GET_IMAGE_CODE = 2;
    int position = 0;
    int positionDetail = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_profile);
        init();
        addListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getProfile(userInfor.getUsername(), userInfor.getUserId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        sliderLayout.removeAllSliders();
        getProfile(userInfor.getUsername(), userInfor.getUserId());
    }

    private void init() {
        Fresco.initialize(ViewMyProfileActivity.this);
        userInfor = RealmContext.getInstance().getUser();
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        tvPost = findViewById(R.id.tv_post);
        sliderLayout = findViewById(R.id.slider);
        tvUsername = findViewById(R.id.tv_username);
        tvAddress = findViewById(R.id.tv_address);
        tvDoB = findViewById(R.id.tv_birthday);
        tvPhone = findViewById(R.id.tv_phone);
        viewFlipper = findViewById(R.id.flipper_status);
        recyclerView = findViewById(R.id.rv_status);
        ivAva = findViewById(R.id.iv_ava);
        newfeedAva = findViewById(R.id.newfeeds_ava);
        tvMenu = findViewById(R.id.tv_menu);
        itemAddress = findViewById(R.id.item_address);
        itemPhone = findViewById(R.id.item_phone);
        ivCamera = findViewById(R.id.iv_camera);
        refreshLayout = findViewById(R.id.refresh_layout);
        ivUpdateCover = findViewById(R.id.iv_update_cover);
        storageReference = FirebaseStorage.getInstance().getReference();
        avatarUrl = new ArrayList<>();

        statusArrayList = new ArrayList<>();

        statusAdapter = new StatusAdapter(this, statusArrayList);
        recyclerView.setAdapter(statusAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewMyProfileActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void addListener() {
        ivAva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(avatarUrl.size() != 0){
                    new ImageViewer.Builder(ViewMyProfileActivity.this, avatarUrl)
                            .setStartPosition(0)
                            .show();
                }
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfile(userInfor.getUsername(), userInfor.getUserId());
            }
        });

        ivUpdateCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewMyProfileActivity.this, UpdateCoverActivity.class);
                startActivity(intent);
            }
        });

        tvPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(ViewMyProfileActivity.this);
//                LayoutInflater inflater = ViewMyProfileActivity.this.getLayoutInflater();
//                View dialogView = inflater.inflate(R.layout.layout_dialog_post, null);
//                builder.setView(dialogView);
//                builder.setCancelable(false);
//
//                dialogAvatar = dialogView.findViewById(R.id.dialog_ava);
//                edtPost = dialogView.findViewById(R.id.edt_post);
//
//                builder.setNegativeButton("Cancel", null);
//
//                builder.setPositiveButton("Post", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        String content = edtPost.getText().toString();
//                        if (content.isEmpty()) {
//                            Utils.showToast(ViewMyProfileActivity.this, "You didn't input content to post!");
//                        } else {
////                            createPost(content);
//                        }
//                    }
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
                Intent intent = new Intent(ViewMyProfileActivity.this, CreatePostFromMyProfileActivity.class);
                startActivity(intent);
            }
        });

        tvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(ViewMyProfileActivity.this, tvMenu);
                popupMenu.inflate(R.menu.profile_option_menu);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.option_logout:
                                RealmContext.getInstance().deleteAllUser();
                                gotoLogin();
                                break;
                            case R.id.option_edit_profile:
                                Intent intent = new Intent(ViewMyProfileActivity.this, UpdateProfileActivity.class);
                                startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
            }
        });

        itemAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loc = tvAddress.getText().toString();
                Uri addressUri = Uri.parse("geo:0,0?q=" + loc);
                Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);
                startActivity(intent);
            }
        });

        itemPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = tvPhone.getText().toString();
                if (!TextUtils.isEmpty(tel)) {
                    String dial = "tel:" + tel;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
                }
            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ensurePermission();
            }
        });
    }

//    private void createPost(String content) {
//        CreateStatusSendForm sendForm = new CreateStatusSendForm(userInfor.getUserId(), content);
//        retrofitService.createPost(sendForm).enqueue(new Callback<Status>() {
//            @Override
//            public void onResponse(Call<Status> call, Response<Status> response) {
//                Status status = response.body();
//                if (response.code() == 200 && status != null) {
//                    statusArrayList.add(0, status);
//                    statusAdapter.notifyDataSetChanged();
//                    edtPost.setText("");
//                } else {
//                    Utils.showToast(ViewMyProfileActivity.this, "Post fail!");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Status> call, Throwable t) {
//                Utils.showToast(ViewMyProfileActivity.this, "No internet!");
//            }
//        });
//    }

    private void ensurePermission(){
        listPermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if(checkPermission(ViewMyProfileActivity.this, listPermissions)){
            openGallery();
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissions, REQUEST_PERMISSION_CODE);
            }
        }
    }

    private boolean checkPermission(Context context, String[] listPermission) {
        if (context != null && listPermission != null) {
            for (String permission : listPermission) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSION_CODE){
            if(checkPermission(ViewMyProfileActivity.this, listPermissions)){
                openGallery();
            }
        } else{
            Utils.showToast(ViewMyProfileActivity.this, "Request is denied!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_GET_IMAGE_CODE && data != null){
            Uri uri = data.getData();
            ivAva.setImageURI(uri);
            if (uri != null) {
                uploadImage(uri);
            }
        }

//        if(requestCode == CommentActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null){
//            statusArrayList.get(position).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
//            statusArrayList.get(position).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
//            statusArrayList.get(position).setNumberComment(data.getIntExtra(Constant.GetNumberComment, 0));
//            statusAdapter.notifyItemChanged(position);
//        }
//
//        if(requestCode == DetailPostActivity.REQUEST_CODE_DETAIL && resultCode == Activity.RESULT_OK && data != null){
//            statusArrayList.get(position).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
//            statusArrayList.get(position).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
//            statusArrayList.get(position).setNumberComment(Integer.parseInt(data.getStringExtra(Constant.GetNumberComment)));
//            statusAdapter.notifyItemChanged(positionDetail);
//        }
    }

    private void uploadImage(Uri uri) {
        StorageReference reference = storageReference.child("avatar/" + uri.getLastPathSegment());
        UploadTask uploadTask = reference.putFile(uri);

        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return reference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    Utils.showToast(ViewMyProfileActivity.this, "Done!");
//                    Log.d("bkhub", task.getResult().toString());

                    updateAvatar(task.getResult().toString());
                } else {
                    Utils.showToast(ViewMyProfileActivity.this, "Fail!");
                }
            }
        });
    }

    private void updateAvatar(String avatarUrl){
        Avatar avatarSend = new Avatar(avatarUrl);
        retrofitService.updateAvatar(userInfor.getUserId(), avatarSend).enqueue(new Callback<Avatar>() {
            @Override
            public void onResponse(Call<Avatar> call, Response<Avatar> response) {
                Avatar avatarRes = response.body();
                if(response.code() == 200 && avatarRes != null){
                    RealmContext.getInstance().updateAvartar(avatarRes.getAvatarUrl());
                } else {
                    Utils.showToast(ViewMyProfileActivity.this, "This is fail while getting image!");
                }
            }

            @Override
            public void onFailure(Call<Avatar> call, Throwable t) {
                Utils.showToast(ViewMyProfileActivity.this, "No Internet!");
            }
        });
    }

    private void openGallery(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GET_IMAGE_CODE);
    }

    private void gotoLogin() {
        Intent intent = new Intent(ViewMyProfileActivity.this, AuthenActivity.class);
        startActivity(intent);
        ViewMyProfileActivity.this.finish();
    }

    @Override
    public void onLikeClick(int position, Status status) {
        likePost(position, status);
    }

    @Override
    public void onCommentClick(int position, Status status) {
        position = this.position;
        Intent intent = new Intent(ViewMyProfileActivity.this, CommentActivity.class);
        intent.putExtra(Constant.GetStatusForDetail, status);
//        startActivity(intent);
        startActivityForResult(intent, CommentActivity.REQUEST_CODE);
    }

    private void likePost(int position, Status status) {
        LikeStatusSendForm sendForm = new LikeStatusSendForm(userInfor.getUserId(), status.getPostId());
        retrofitService.likePost(sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    status.setLike(!status.isLike());
                    if (status.isLike()) {
                        status.setNumberLike(status.getNumberLike() + 1);
                    } else {
                        status.setNumberLike(status.getNumberLike() - 1);
                    }
                    statusAdapter.notifyItemChanged(position);
                } else {
                    Utils.showToast(ViewMyProfileActivity.this, "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(ViewMyProfileActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onEditStatus(Status status) {
        ArrayList<String> images = new ArrayList<>();
        images.addAll(status.getImages());
        Intent intent = new Intent(ViewMyProfileActivity.this, UpdatePostFromMyProfileActivity.class);
//        intent.putExtra("GetContent", status.getContent());
//        intent.putStringArrayListExtra("GetImages", images);
//        intent.putExtra("GetPostId", status.getPostId());
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivity(intent);
    }

    @Override
    public void onDeleteStatus(Status status) {
        new AlertDialog.Builder(ViewMyProfileActivity.this)
                .setTitle("Delete status")
                .setMessage("Do you sure to delete this status?")
                .setIcon(R.drawable.icon_trash)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteStatus(userInfor.getUserId(), status);
                        Utils.showToast(ViewMyProfileActivity.this, "Done!");
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @Override
    public void onDetailImage(int positionAdapter, int positionImage, Status status) {
        this.positionDetail = positionAdapter;
        Intent intent = new Intent(ViewMyProfileActivity.this, DetailPostActivity.class);
        intent.putExtra(Constant.GetPositionImage, positionImage);
        intent.putExtra(Constant.GetPositionAdapterForDetail, positionAdapter);
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivityForResult(intent, DetailPostActivity.REQUEST_CODE_DETAIL);
//        startActivity(intent);
    }

    public void deleteStatus(String userId, Status status) {
        retrofitService.deleteStatus(status.getPostId(), userId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    statusArrayList.remove(status);
//                    statusProfileAdapter.notifyDataSetChanged();
                    statusAdapter.notifyDataSetChanged();
                    Utils.showToast(ViewMyProfileActivity.this, "Done!");
                } else Utils.showToast(ViewMyProfileActivity.this, "Fail!");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(ViewMyProfileActivity.this, "No Internet!");
            }
        });
    }

    @Override
    public void onSaveClick(String newContent) {
//        updateStatus(userInfor.getUserId(), newContent, currentStatus.getPostId());
    }

//    public void updateStatus(String userId, String newContent, String postId) {
//        UpdateStatusSendForm sendForm = new UpdateStatusSendForm(userId, newContent);
//
//        retrofitService.updateStatus(postId, sendForm).enqueue(new Callback<Status>() {
//            @Override
//            public void onResponse(Call<Status> call, Response<Status> response) {
//                Status res = response.body();
//                if (response.code() == 200 && res != null) {
//                    currentStatus.setContent(res.getContent());
////                    statusProfileAdapter.notifyDataSetChanged();
//                    statusAdapter.notifyDataSetChanged();
//                    Utils.showToast(ViewMyProfileActivity.this, "Done!");
//                } else Utils.showToast(ViewMyProfileActivity.this, "Fail!");
//            }
//
//            @Override
//            public void onFailure(Call<Status> call, Throwable t) {
//                Utils.showToast(ViewMyProfileActivity.this, "No Internet!");
//            }
//        });
//    }

    private void getProfile(String username, String userId) {
        retrofitService.getProfileUser(username, userId).enqueue(new Callback<ProfileUser>() {
            @Override
            public void onResponse(Call<ProfileUser> call, Response<ProfileUser> response) {
                ProfileUser profileUser = response.body();
                if (response.code() == 200 && profileUser != null) {
//                    Log.d("bkhub", profileUser.toString());
                    tvUsername.setText(profileUser.getFullName());
                    tvAddress.setText(profileUser.getAddress());
                    tvDoB.setText(profileUser.getBirthday());
                    tvPhone.setText(profileUser.getPhone());
                    if(!userInfor.getAvatar().equals("")){
                        Glide.with(ViewMyProfileActivity.this).load(userInfor.getAvatar()).into(ivAva);
                        Glide.with(ViewMyProfileActivity.this).load(userInfor.getAvatar()).into(newfeedAva);
                        avatarUrl.add(userInfor.getAvatar());
                    } else {
                        ivAva.setBackgroundResource(R.drawable.icon_person_big);
                        newfeedAva.setBackgroundResource(R.drawable.icon_person_big);
                    }
                    address = profileUser.getAddress();
                    DoB = profileUser.getBirthday();
                    phone = profileUser.getPhone();

                    ArrayList<Status> statuses = profileUser.getPostList();
                    if (statuses != null) {
                        statusArrayList.clear();
                        statusArrayList.addAll(statuses);
//                        statusProfileAdapter.notifyDataSetChanged();\
                        statusAdapter.notifyDataSetChanged();
                        viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
                    } else {
                        viewFlipper.setDisplayedChild(MODE_NODATA);
                    }

//                    String[] coverPhoto = profileUser.getCoverPhoto();
                    showSlider(profileUser.getCoverPhoto());
                } else {
                    Utils.showToast(ViewMyProfileActivity.this, "Fail!");
                }
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ProfileUser> call, Throwable t) {
                Utils.showToast(ViewMyProfileActivity.this, "No Internet!");
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void showSlider(String[] coverPhoto) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        for (String url : coverPhoto) {
//            TextSliderView textSliderView = new TextSliderView(getContext());
            DefaultSliderView textSliderView = new DefaultSliderView(ViewMyProfileActivity.this);
            textSliderView
                    .image(url)
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .bundle(new Bundle());

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);//đặt hiệu ứng
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);//dấu 3 chấm nằm ở trung tâm ảnh
        sliderLayout.setDuration(4000);//đặt thời gian tự động chuyển ảnh
    }

}

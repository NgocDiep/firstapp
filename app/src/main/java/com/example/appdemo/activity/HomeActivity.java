package com.example.appdemo.activity;

import android.animation.LayoutTransition;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.example.appdemo.R;
import com.example.appdemo.adapter.HomeViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    HomeViewPagerAdapter adapter;
    EditText edtSearch;
    boolean expand = false;
    ViewGroup viewGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_new_feed);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_youtube);
        tabLayout.getTabAt(2).setIcon(R.drawable.icon_mess);
        tabLayout.getTabAt(3).setIcon(R.drawable.icon_list_black);
        tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
    }

    private void init() {
        tabLayout = findViewById(R.id.tab);
        viewPager = findViewById(R.id.viewPager);
        viewGroup = findViewById(R.id.root);

        viewGroup.getLayoutTransition().enableTransitionType(LayoutTransition.APPEARING);
        edtSearch = findViewById(R.id.edt_search);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expand){
                    expand = false;
                    edtSearch.setHint(R.string.search);
                } else {
                    expand = true;
                    edtSearch.setHint(R.string.searchClick);
                }
            }
        });

        //load đồng thời 4 trang ngay từ đầu, nhưng không load lại khi chuyển trang, trừ khi refresh lại trang.
//        viewPager.setOffscreenPageLimit(4);

        adapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.white);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.black);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
    }
}

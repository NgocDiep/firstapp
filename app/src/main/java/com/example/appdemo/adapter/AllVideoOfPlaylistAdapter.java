package com.example.appdemo.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.interf.OnVideoClickListener;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllVideoOfPlaylistAdapter extends RecyclerView.Adapter<AllVideoOfPlaylistAdapter.MyViewHolder> {
    ArrayList<JsonObject> objectArrayList;
    private OnVideoClickListener listener;

    public AllVideoOfPlaylistAdapter(ArrayList<JsonObject> objectArrayList, OnVideoClickListener listener) {
        this.objectArrayList = objectArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AllVideoOfPlaylistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_video, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllVideoOfPlaylistAdapter.MyViewHolder holder, int position) {
        holder.bindView(objectArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return objectArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_image)
        ImageView ivImage;
        @BindView(R.id.tv_video_name)
        TextView tvVideoName;
        @BindView(R.id.tv_channel_name)
        TextView tvChannelName;
        Context context;
        @BindView(R.id.item_video)
        LinearLayout itemVideo;
        String videoId, publishedAt, title, description, channelTitle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

            itemVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.GetVideoId, videoId);
                    bundle.putString(Constant.GetChannelTitle, channelTitle);
                    bundle.putString(Constant.GetPublishedAt, publishedAt);
                    bundle.putString(Constant.GetDescription, description);
                    bundle.putString(Constant.GetTitle, title);
                    listener.onVideoClick(bundle);
                }
            });
        }

        private void bindView(JsonObject jsonObject){
            JsonObject snippetObject = jsonObject.get("snippet").getAsJsonObject();
            tvVideoName.setText(snippetObject.getAsJsonObject().get("title").getAsString());
            tvChannelName.setText(snippetObject.getAsJsonObject().get("channelTitle").getAsString());

            JsonObject thumbnailsObject = snippetObject.get("thumbnails").getAsJsonObject();
            JsonObject defaultObject = thumbnailsObject.get("medium").getAsJsonObject();
            Glide.with(context).load(defaultObject.get("url").getAsString()).into(ivImage);

            JsonObject resourceId = snippetObject.get("resourceId").getAsJsonObject();
            String videoId = resourceId.get("videoId").getAsString();
            this.videoId = videoId;

            String publishedAt = snippetObject.get("publishedAt").getAsString();
            this.publishedAt = publishedAt;

            String title = snippetObject.get("title").getAsString();
            this.title = title;

            String description = snippetObject.get("description").getAsString();
            this.description = description;

            String channelTitle = snippetObject.get("channelTitle").getAsString();
            this.channelTitle = channelTitle;
        }
    }
}

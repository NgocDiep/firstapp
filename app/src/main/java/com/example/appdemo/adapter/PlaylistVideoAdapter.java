package com.example.appdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appdemo.R;
import com.example.appdemo.interf.OnItemPlaylistClickListener;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class PlaylistVideoAdapter extends RecyclerView.Adapter<PlaylistVideoAdapter.MyViewHolder> {
    ArrayList<JsonObject> jsonObjects;
    private OnItemPlaylistClickListener listener;
    public static int numberVideo = 0;

    public PlaylistVideoAdapter(ArrayList<JsonObject> jsonObjects, OnItemPlaylistClickListener listener) {
        this.jsonObjects = jsonObjects;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PlaylistVideoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_playlist, parent, false);
        return new PlaylistVideoAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistVideoAdapter.MyViewHolder holder, int position) {
        holder.bindView(jsonObjects.get(position));
    }

    @Override
    public int getItemCount() {
        numberVideo = jsonObjects.size();
        return jsonObjects.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_image)
        ImageView ivImage;
        @BindView(R.id.tv_number_playlist1)
        TextView tvNumberPlaylist1;
        @BindView(R.id.tv_number_playlist2)
        TextView tvNumberPlaylist2;
        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;
        @BindView(R.id.tv_channel_name)
        TextView tvChannelName;
        Context context;
        @BindView(R.id.item_playlist)
        LinearLayout itemPlaylist;
        String playlistId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

            itemPlaylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPlaylistClick(playlistId);
                }
            });
        }

        private void bindView(JsonObject jsonObject){
            tvNumberPlaylist1.setText(valueOf(numberVideo));
            tvNumberPlaylist2.setText(numberVideo + " video");

            JsonObject snippetObject = jsonObject.get("snippet").getAsJsonObject();
            tvPlaylistName.setText(snippetObject.getAsJsonObject().get("title").getAsString());
            tvChannelName.setText(snippetObject.getAsJsonObject().get("channelTitle").getAsString());

            JsonObject thumbnailsObject = snippetObject.get("thumbnails").getAsJsonObject();
            JsonObject defaultObject = thumbnailsObject.get("medium").getAsJsonObject();
            Glide.with(context).load(defaultObject.get("url").getAsString()).into(ivImage);

            String playlistId = jsonObject.get("id").getAsString();
            this.playlistId = playlistId;
        }
    }
}

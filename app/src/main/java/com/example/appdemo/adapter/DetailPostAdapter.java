package com.example.appdemo.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appdemo.R;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.utils.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.photodraweeview.PhotoDraweeView;

public class DetailPostAdapter extends RecyclerView.Adapter<DetailPostAdapter.MyViewHolder> {
    List<String> imageList;

    public DetailPostAdapter(List<String> imageList) {
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public DetailPostAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_image_detail, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailPostAdapter.MyViewHolder holder, int position) {
        holder.bindView(imageList.get(position));
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
//        @BindView(R.id.iv_horizontal)
//        ImageView ivHorizontal;
//        @BindView(R.id.iv_vertical)
//        ImageView ivVertical;
//        @BindView(R.id.iv_square)
//        ImageView ivSquare;
        @BindView(R.id.iv_image)
        ImageView ivImage;
//        PhotoDraweeView ivImage;
        Context context;
//        int width, height;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();

            ButterKnife.bind(this, itemView);
            Fresco.initialize(context);
        }

        private void bindView(String uri){
            Glide.with(context).load(uri).into(ivImage);
//            ivImage.setPhotoUri(Uri.parse(uri));

            ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ImageViewer.Builder(context, imageList)
                            .setStartPosition(getAdapterPosition())
                            .show();
//                    new ImageViewer.Builder<>(this, imageList)
//                            .setStartPosition(startPosition)
//                            .hideStatusBar(false)
//                            .allowZooming(true)
//                            .allowSwipeToDismiss(true)
//                            .setBackgroundColorRes(colorRes)
//                            //.setBackgroundColor(color)
//                            .setImageMargin(margin)
//                            //.setImageMarginPx(marginPx)
//                            .setContainerPadding(this, dimen)
//                            //.setContainerPadding(this, dimenStart, dimenTop, dimenEnd, dimenBottom)
//                            //.setContainerPaddingPx(padding)
//                            //.setContainerPaddingPx(start, top, end, bottom)
//                            .setCustomImageRequestBuilder(imageRequestBuilder)
//                            .setCustomDraweeHierarchyBuilder(draweeHierarchyBuilder)
//                            .setImageChangeListener(imageChangeListener)
//                            .setOnDismissListener(onDismissListener)
//                            .setOverlayView(overlayView)
//                            .show();
                }
            });
        }
    }
}

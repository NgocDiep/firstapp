package com.example.appdemo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.activity.AllVideoOfPlaylistActivity;
import com.example.appdemo.adapter.PlaylistVideoAdapter;
import com.example.appdemo.interf.OnItemPlaylistClickListener;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFragment extends Fragment implements OnItemPlaylistClickListener {
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.rv_playlist)
    RecyclerView recyclerView;
    final int MODE_RECYCLEVIEW = 1;
    ArrayList<JsonObject> playlistArrayList;
    PlaylistVideoAdapter playlistAdapter;
    private RetrofitService retrofitService;

    public VideoFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        init(view);
        getPlaylistVideo();
        return view;
    }

    private void init(View view) {
        ButterKnife.bind(this, view);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_YOUTUBE).createService(RetrofitService.class);

        playlistArrayList = new ArrayList<>();
        playlistAdapter = new PlaylistVideoAdapter(playlistArrayList, this::onPlaylistClick);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(playlistAdapter);
    }

    private void getPlaylistVideo() {
        retrofitService.getPlaylistVideo(Constant.part, Constant.channelId, Constant.API_KEY).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonObject = response.body();
                if (response.code() == 200 && jsonObject != null) {
                    JsonArray jsonPlaylistArray = jsonObject.getAsJsonArray("items");
                    for (int i = 0; i < jsonPlaylistArray.size(); i++) {
                        playlistArrayList.add(jsonPlaylistArray.get(i).getAsJsonObject());
                    }
                    playlistAdapter.notifyDataSetChanged();
                    viewFlipper.setDisplayedChild(MODE_RECYCLEVIEW);
                } else {
                    Utils.showToast(getActivity(), "Fail!");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utils.showToast(getActivity(), "No Internet!");
            }
        });
    }

    @Override
    public void onPlaylistClick(String playlistId) {
        Intent intent = new Intent(getActivity(), AllVideoOfPlaylistActivity.class);
        intent.putExtra(Constant.GetPlaylistId, playlistId);
        startActivity(intent);
    }
}

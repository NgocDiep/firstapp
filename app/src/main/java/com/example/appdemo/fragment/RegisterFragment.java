package com.example.appdemo.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;

import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;

public class RegisterFragment extends Fragment {
    EditText edtUsername, edtPassword, edtRePassword, edtFullname, edtDoB, edtAdress, edtPhone;
    Button btnRegister;
    String username, password, fullName, birthday, address, phone;
    ViewFlipper viewFlipper;
    private RetrofitService retrofitService;
    final int MODE_PROCESSBAR = 1;
    final int MODE_BUTTON = 0;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        edtUsername = view.findViewById(R.id.edtUsername);
        edtPassword = view.findViewById(R.id.edtPass);
        edtRePassword = view.findViewById(R.id.re_edtPass);
        edtFullname = view.findViewById(R.id.edtFullName);
        edtDoB = view.findViewById(R.id.edtDoB);
        edtAdress = view.findViewById(R.id.edtAdress);
        edtPhone = view.findViewById(R.id.edtPhone);
        btnRegister = view.findViewById(R.id.btnRegister);
        viewFlipper = view.findViewById(R.id.view_flipper);
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);

        addListener();
        return view;
    }

    private void addListener() {
//        btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewFlipper.setDisplayedChild(MODE_PROCESSBAR);
//
//                username = edtUsername.getText().toString();
//                password = edtPassword.getText().toString();
//                fullName = edtFullname.getText().toString();
//                birthday = edtDoB.getText().toString();
//                address = edtAdress.getText().toString();
//                phone = edtPhone.getText().toString();
//                if(!password.equals(edtRePassword.getText().toString())){
//                    Utils.showToast(getActivity(), "Password isn't match!");
//                } else {
//                    RegisterSendForm sendForm = new RegisterSendForm(username, password, fullName, birthday, address, phone);
//                    retrofitService.register(sendForm).enqueue(new Callback<UserInfor>() {
//                        @Override
//                        public void onResponse(Call<UserInfor> call, Response<UserInfor> response) {
//                            UserInfor userInfor = response.body();
//                            if(response.code() == 200 && userInfor != null){
//                                RealmContext.getInstance().deleteAllUser();
//
//                                RealmContext.getInstance().addUser(userInfor);
//                                Intent intent = new Intent(getActivity(), HomeActivity.class);
//                                startActivity(intent);
//                                getActivity().finish();
//                            } else {
//                                Utils.showToast(getActivity(), "Fail!");
//                            }
//                            viewFlipper.setDisplayedChild(MODE_BUTTON);
//                        }
//
//                        @Override
//                        public void onFailure(Call<UserInfor> call, Throwable t) {
//                            Utils.showToast(getActivity(), "No Internet!");
//                            viewFlipper.setDisplayedChild(MODE_BUTTON);
//                        }
//                    });
//                }
//            }
//        });
    }


}

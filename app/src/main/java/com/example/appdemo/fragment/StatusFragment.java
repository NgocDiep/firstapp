package com.example.appdemo.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.example.appdemo.Constant;
import com.example.appdemo.R;
import com.example.appdemo.activity.CommentActivity;
import com.example.appdemo.activity.CreatePostActivity;
import com.example.appdemo.activity.DetailPostActivity;
import com.example.appdemo.activity.UpdatePostActivity;
import com.example.appdemo.activity.ViewMyProfileActivity;
import com.example.appdemo.adapter.StatusAdapter;
import com.example.appdemo.adapter.StoryAdapter;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.interf.OnItemStatusClickListener;
import com.example.appdemo.interf.OnUpdateDialogListener;
import com.example.appdemo.json_models.request.LikeStatusSendForm;
import com.example.appdemo.json_models.response.Status;
import com.example.appdemo.json_models.response.UserInfor;
import com.example.appdemo.network.RetrofitService;
import com.example.appdemo.network.RetrofitUtils;
import com.example.appdemo.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusFragment extends Fragment implements OnItemStatusClickListener, OnUpdateDialogListener {
    private RetrofitService retrofitService;
    ViewFlipper viewFlipper, flipperStory;
    RecyclerView recyclerView, rvStory;
    TextView tvPost;
    UserInfor user;
    CircleImageView newfeesAvatar;

    StatusAdapter statusAdapter;
    ArrayList<Status> statusList;
    StoryAdapter storyAdapter;
    ArrayList<UserInfor> storyList;

    final int MODE_NO_DATA = 1;
    final int MODE_RECYCYCLEVIEW = 2;
    SwipeRefreshLayout refreshLayout;
    int position = 0;
    int positionDetail = 0;

    public StatusFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        user = RealmContext.getInstance().getUser();
        Log.d("123", user.toString());

        init(view);
        if (user != null) {
            Glide.with(getActivity()).load(user.getAvatar()).into(newfeesAvatar);
            getAllFriend();
            getAllPost(user.getUserId());
        }
        addListener();
    }

    @Override
    public void onResume() {
        super.onResume();
//        getAllPost(user.getUserId());
//        statusAdapter.notifyItemChanged(position);
//        statusAdapter.notifyItemChanged(positionDetail);
    }

    private void init(View view) {
        retrofitService = RetrofitUtils.getInstance(Constant.TYPE_BKHUB).createService(RetrofitService.class);
        viewFlipper = view.findViewById(R.id.flipper_status);
        tvPost = view.findViewById(R.id.tv_post);
        recyclerView = view.findViewById(R.id.rv_status);
        newfeesAvatar = view.findViewById(R.id.newfeeds_ava);
        refreshLayout = view.findViewById(R.id.refresh_layout);
        rvStory = view.findViewById(R.id.rv_story);
        flipperStory = view.findViewById(R.id.flipper_story);

        storyList = new ArrayList<>();
        storyAdapter = new StoryAdapter(storyList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvStory.setLayoutManager(layoutManager);
        rvStory.setAdapter(storyAdapter);

        statusList = new ArrayList<>();
        statusAdapter = new StatusAdapter(this, statusList);
        recyclerView.setAdapter(statusAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getAllFriend(){
        retrofitService.getAllFriend(user.getUserId()).enqueue(new Callback<ArrayList<UserInfor>>() {
            @Override
            public void onResponse(Call<ArrayList<UserInfor>> call, Response<ArrayList<UserInfor>> response) {
                ArrayList<UserInfor> userInfors = response.body();
                if(response.code() == 200 && userInfors != null){
                    storyList.clear();
                    storyList.addAll(userInfors);
                    storyAdapter.notifyDataSetChanged();
                    flipperStory.setDisplayedChild(1);
                } else {
                    flipperStory.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserInfor>> call, Throwable t) {
                Utils.showToast(getActivity(), "No Internet!");
            }
        });
    }

    private void addListener() {
        newfeesAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewMyProfileActivity.class);
                startActivity(intent);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllPost(user.getUserId());
            }
        });

        tvPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreatePostActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getAllPost(String userId) {
        retrofitService.getAllPost(userId).enqueue(new Callback<List<Status>>() {
            @Override
            public void onResponse(Call<List<Status>> call, Response<List<Status>> response) {
                ArrayList<Status> statuses = (ArrayList<Status>) response.body();
                if (response.code() == 200 && statusList != null) {
                    statusList.clear();
                    statusList.addAll(statuses);
                    statusAdapter.notifyDataSetChanged();
                    viewFlipper.setDisplayedChild(MODE_RECYCYCLEVIEW);
                } else {
                    viewFlipper.setDisplayedChild(MODE_NO_DATA);
                }
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Status>> call, Throwable t) {
                Utils.showToast(getActivity(), "No Internet!");
                refreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onLikeClick(int position, Status status) {
        likePost(position, status);
    }

    @Override
    public void onCommentClick(int position, Status status) {
        this.position = position;
        Intent intent = new Intent(getActivity(), CommentActivity.class);
//        startActivity(intent);
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivityForResult(intent, CommentActivity.REQUEST_CODE);
        //ĐOẠN NÀY NÊN DÙNG ACTIVITY FOR RESULT ĐỂ TỐI ƯU VÀ GIẢM TẢI LÊN API
    }


    @Override
    public void onEditStatus(Status status) {
//        currentStatus = status;
//        EditStatusDialog dialog = new EditStatusDialog(getContext(), this);
//        dialog.setContent(status.getContent());
//        dialog.show();
        ArrayList<String> images = new ArrayList<>();
        images.addAll(status.getImages());
        Intent intent = new Intent(getActivity(), UpdatePostActivity.class);
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommentActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            statusList.get(position).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
            statusList.get(position).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
            statusList.get(position).setNumberComment(data.getIntExtra(Constant.GetNumberComment, 0));
            statusAdapter.notifyItemChanged(position);
        }

        if (requestCode == DetailPostActivity.REQUEST_CODE_DETAIL && resultCode == Activity.RESULT_OK && data != null) {
            statusList.get(positionDetail).setLike(data.getBooleanExtra(Constant.GetIsLike, false));
            statusList.get(positionDetail).setNumberLike(Integer.parseInt(data.getStringExtra(Constant.GetNumberLike)));
            statusList.get(positionDetail).setNumberComment(Integer.parseInt(data.getStringExtra(Constant.GetNumberComment)));
            statusAdapter.notifyItemChanged(positionDetail);
        }
    }

    @Override
    public void onDeleteStatus(Status status) {
        new AlertDialog.Builder(getContext())
                .setTitle("Delete status")
                .setMessage("Do you sure to delete this status?")
                .setIcon(R.drawable.icon_trash)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteStatus(user.getUserId(), status);
                        Utils.showToast(getActivity(), "Done!");
                    }
                })
                .setNegativeButton("CANCEL", null)
                .show();
    }

    @Override
    public void onDetailImage(int positionAdapter, int positionImage, Status status) {
        this.positionDetail = positionAdapter;
        Intent intent = new Intent(getActivity(), DetailPostActivity.class);
        intent.putExtra(Constant.GetPositionImage, positionImage);
        intent.putExtra(Constant.GetPositionAdapterForDetail, positionAdapter);
        intent.putExtra(Constant.GetStatusForDetail, status);
        startActivityForResult(intent, DetailPostActivity.REQUEST_CODE_DETAIL);
    }


    public void deleteStatus(String userId, Status status) {
        retrofitService.deleteStatus(status.getPostId(), userId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    statusList.remove(status);
                    statusAdapter.notifyDataSetChanged();
                    Utils.showToast(getContext(), "Done!");
                } else Utils.showToast(getContext(), "Fail!");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(getContext(), "No Internet!");
            }
        });
    }

    private void likePost(int position, Status status) {
        LikeStatusSendForm sendForm = new LikeStatusSendForm(user.getUserId(), status.getPostId());
        retrofitService.likePost(sendForm).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    status.setLike(!status.isLike());
                    if (status.isLike()) {
                        status.setNumberLike(status.getNumberLike() + 1);
                    } else {
                        status.setNumberLike(status.getNumberLike() - 1);
                    }
                    statusAdapter.notifyItemChanged(position);
                } else {
                    Utils.showToast(getActivity(), "Fail!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Utils.showToast(getActivity(), "No Internet!");
            }
        });
    }

//    public void updateStatus(String userId, String newContent, String postId){
//        UpdateStatusSendForm sendForm = new UpdateStatusSendForm(userId, newContent);
//
//        retrofitService.updateStatus(postId, sendForm).enqueue(new Callback<Status>() {
//            @Override
//            public void onResponse(Call<Status> call, Response<Status> response) {
//                Status res = response.body();
//                if(response.code() == 200 && res != null){
//                    currentStatus.setContent(res.getContent());
//                    statusAdapter.notifyDataSetChanged();
//                    Utils.showToast(getActivity(), "Done!");
//                } else Utils.showToast(getActivity(), "Fail!");
//            }
//
//            @Override
//            public void onFailure(Call<Status> call, Throwable t) {
//                Utils.showToast(getActivity(), "No Internet!");
//            }
//        });
//    }

    @Override
    public void onSaveClick(String newContent) {
//        updateStatus(user.getUserId(), newContent, currentStatus.getPostId());
    }
}

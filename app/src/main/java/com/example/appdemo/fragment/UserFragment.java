package com.example.appdemo.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.appdemo.R;
import com.example.appdemo.activity.AuthenActivity;
import com.example.appdemo.activity.FriendActivity;
import com.example.appdemo.activity.UpdateProfileActivity;
import com.example.appdemo.activity.ViewMyProfileActivity;
import com.example.appdemo.dbcontext.RealmContext;
import com.example.appdemo.json_models.response.UserInfor;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserFragment extends Fragment {
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.iv_ava)
    CircleImageView ivAva;
    UserInfor userInfor;
    @BindView(R.id.layout_edit_profile)
    LinearLayout itemEditProfile;
    @BindView(R.id.relative_layout_user)
    RelativeLayout itemViewProfile;
    @BindView(R.id.layout_friends)
    LinearLayout itemFriends;
    @BindView(R.id.layout_logout)
    LinearLayout itemLogout;
    String address, DoB, phone;

    public UserFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        init(view);
        addListener();
        return view;
    }

    private void init(View view) {
        ButterKnife.bind(this, view);
        userInfor = RealmContext.getInstance().getUser();
        tvUsername.setText(userInfor.getFullName());
        Glide.with(getContext()).load(userInfor.getAvatar()).into(ivAva);
    }

    private void addListener(){
        itemViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewMyProfileActivity.class);
                startActivity(intent);
            }
        });

        itemEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
                intent.putExtra("GetFullName", userInfor.getFullName());
                intent.putExtra("GetAddress", address);
                intent.putExtra("GetDoB", DoB);
                intent.putExtra("GetPhone", phone);
                startActivity(intent);
            }
        });

        itemFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FriendActivity.class);
                startActivity(intent);
            }
        });

        itemLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RealmContext.getInstance().deleteAllUser();
                Intent intent = new Intent(getActivity(), AuthenActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }
}
